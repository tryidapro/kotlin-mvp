package com.example.rootproject.network

import com.example.rootproject.DB.CRUDInterface
import com.example.rootproject.model.ServerResponse
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class ApiDataClient : ApiInterface {

    val BASE_URL : String = "https://samples.openweathermap.org/data/2.5/"

    var apiClient : ApiMethodsInterface

    var dbClient : CRUDInterface

    @Inject
    constructor(dbClient : CRUDInterface) {
        this.dbClient = dbClient
        apiClient = createApiClient(httpLogger())
    }

    fun createApiClient(client :OkHttpClient ) : ApiMethodsInterface {
        val jackson  = ObjectMapper()
        jackson.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        val retrofit : Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create(jackson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
        return retrofit.create(ApiMethodsInterface::class.java)
    }

    fun httpLogger() : OkHttpClient {
        val builder = OkHttpClient().newBuilder()
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        builder.addInterceptor(interceptor)
        return builder.build()
    }

    override fun loadForecast(city: String) : Observable<ServerResponse> {
        var map = HashMap<String, String>()
        map.put("id", city)
        map.put("appid", "test")
        return apiClient.loadForecast(map).map({saveData(it)}).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    private fun saveData(response: ServerResponse) : ServerResponse {
     //   dbClient.saveCityInfo(response.city)
        return response
    }
}