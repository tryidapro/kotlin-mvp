package com.example.rootproject.network

import com.example.rootproject.model.ServerResponse
import io.reactivex.Observable

interface ApiInterface {

    fun loadForecast(city: String): Observable<ServerResponse>

}
