package com.example.rootproject.network

import com.example.rootproject.model.ServerResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiMethodsInterface {

    @GET("forecast/daily")
   fun loadForecast(@QueryMap params:Map<String, String>): Observable<ServerResponse>
}