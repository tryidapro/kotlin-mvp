package com.example.rootproject.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rootproject.R
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.TaskModel.TaskShort

class HomeMainAdapter() : BaseRecyclerAdapter<HomeWidgetTasks>() {

    var context: Context? = null

    constructor(context: Context?):this(){

        this.context = context
    }

    lateinit var listener: WidgetItemEvents

    private var lastLoaded = 0



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.home_widget_frg, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(position)    }

      fun setOnTaskClickListener(listener: WidgetItemEvents) {
        this.listener = listener
    }


    interface WidgetItemEvents {
        fun onItemClick(type: String, task: TaskShort)
    }

     inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         var title : TextView
         var widgetRv: RecyclerView

        init {
            title = itemView.findViewById(R.id.widgetTitle)
            widgetRv = itemView.findViewById(R.id.widgetRv)
        }

        fun bind(position: Int) {
           title.text= getItem(position).title
           val manager = LinearLayoutManager(context)
            manager.orientation = RecyclerView.HORIZONTAL
            widgetRv.layoutManager = manager
            val adapter = HomeWidgetAdapter()
            adapter.items = getItem(position).list
            adapter.setOnTaskClickListener(object : HomeWidgetAdapter.WidgetClickListener {
                override fun onWidgetClick(task: TaskShort) {
                    listener.onItemClick(getItem(position).title,task)
                }
            })
            widgetRv.adapter = adapter

        }
    }
}