package com.example.rootproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rootproject.R
import com.example.rootproject.model.TaskModel.TaskShort

class HomeWidgetAdapter : BaseRecyclerAdapter<TaskShort>() {


    lateinit var listener: WidgetClickListener

    private var lastLoaded = 0



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.home_widget_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(position)
    }

      fun setOnTaskClickListener(listener: WidgetClickListener) {
        this.listener = listener
    }


    interface WidgetClickListener {
        fun onWidgetClick(task: TaskShort)

    }

     inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView
        var txtNumber: TextView

        init {
            txtTitle = itemView.findViewById(R.id.ownerTitle)
            txtNumber = itemView.findViewById(R.id.ownerNumber)
        }

        fun bind(position: Int) {
            val task = getItem(position)
            txtTitle.text = task.owner
            txtNumber.text = task.number.toString()
            itemView.setOnClickListener { it -> listener.onWidgetClick(task) }
        }
    }
}