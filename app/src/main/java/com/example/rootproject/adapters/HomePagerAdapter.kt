package com.example.rootproject.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.rootproject.ui.base.BaseFragment
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.rootproject.R
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.ui.home.HomeTabItemFragment
import com.example.rootproject.ui.home.NewTaskFragment


class HomePagerAdapter(fm: FragmentManager, private val context: Context?) : FragmentStatePagerAdapter(fm) {

    private val data: MutableList<BaseFragment>


    init {
        data = ArrayList()
        data.add(HomeTabItemFragment.newInstance(HomeTabList.TabsType.home))
        data.add(HomeTabItemFragment.newInstance(HomeTabList.TabsType.work))
        data.add(HomeTabItemFragment.newInstance(HomeTabList.TabsType.shop))
        data.add(HomeTabItemFragment.newInstance(HomeTabList.TabsType.other))
    }


    override fun getItem(position: Int): Fragment {
        return data[position]
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (HomeTabList.TabsType.valueOf(data[position].arguments!!.getString("type"))) {
            HomeTabList.TabsType.home -> return context?.getString(R.string.home)
            HomeTabList.TabsType.work -> return context?.getString(R.string.work)
            HomeTabList.TabsType.shop -> return context?.getString(R.string.shop)
            HomeTabList.TabsType.other -> return context?.getString(R.string.other)
        }
        return context?.getString(R.string.home)
    }

}
