package com.example.rootproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.R

class TasksListAdapter : BaseRecyclerAdapter<TaskInfoTable>() {


    lateinit var listener: TabListEvents

    private var lastLoaded = 0



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.home_item_frg, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(position)    }

      fun setOnTaskClickListener(listener: TabListEvents) {
        this.listener = listener
    }


    interface TabListEvents {
        fun onItemClick(task: TaskInfoTable)

        fun loadMoreTasks()
    }

     inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView
        var taskText: TextView

        init {
            txtTitle = itemView.findViewById(R.id.taskTitle)
            taskText = itemView.findViewById(R.id.taskText)
        }

        fun bind(position: Int) {
            itemView.setOnClickListener { listener.onItemClick(getItem(position)) }
            val task = getItem(position)
            txtTitle.text = task.taskSubj
            taskText.text = task.taskText?.substring(0, Math.min(task.taskText.length, 30))


            if (position == getItemCount() - 11 && position != lastLoaded) {
                try {
                    lastLoaded = position
                    listener.loadMoreTasks()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }
}