package com.example.rootproject.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rootproject.R
import com.example.rootproject.model.TaskModel.Task

class HomeTabRecyclerAdapter : BaseRecyclerAdapter<Task>() {


    lateinit var listener: TabListEvents

    private var lastLoaded = 0



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.home_item_frg, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(position)    }

      fun setOnTaskClickListener(listener: TabListEvents) {
        this.listener = listener
    }


    interface TabListEvents {
        fun onItemClick(task: Task)

        fun loadMoreTasks()
    }

     inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitle: TextView
        var shopLoyaltyText: TextView

        init {
            txtTitle = itemView.findViewById(R.id.taskTitle)
            shopLoyaltyText = itemView.findViewById(R.id.taskText)
        }

        fun bind(position: Int) {
            itemView.setOnClickListener { listener.onItemClick(getItem(position)) }
            val task = getItem(position)
          //  txtTitle.setText(task.taskName)

            if (position == getItemCount() - 11 && position != lastLoaded) {
                try {
                    lastLoaded = position
                    listener.loadMoreTasks()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
    }
}