package com.example.rootproject.ui.home

import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

interface HomeTabItemEvents {

    interface View : BaseView {
        fun onTasksLoaded(items: List<Task>)

        fun setTasks(items: List<Task>)
    }

    interface Presenter : BasePresenter<View> {
        fun loadMoreItems(type: HomeTabList.TabsType)
    }
}