package com.example.rootproject.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rootproject.R
import com.example.rootproject.adapters.HomeMainAdapter
import com.example.rootproject.cicerone.Screens
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.model.TaskModel.TaskShort
import com.example.rootproject.ui.base.BaseFragmentWithPresenter
import com.example.rootproject.ui.main.MainActivity
import kotlinx.android.synthetic.main.home_root_layout.*

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class HomeRootFragment : BaseFragmentWithPresenter<HomeRootEvents.View,HomeRootPresenter>(), HomeRootEvents.View, HomeMainAdapter.WidgetItemEvents{

    var v : View? = null
    lateinit var homeMainAdapter : HomeMainAdapter


    override fun onForecastLoaded(city: String) {
    }

    private var color = R.color.primary_dark_material_dark


    companion object {
        fun newInstance(): HomeRootFragment {
            val fragment = HomeRootFragment()
            return fragment
        }
    }



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.home_root_layout, null)
        }

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        initActionbar()
        initBottomButton()
        //TODO refactor in in the right way
        init()
        }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun showError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }


    override fun getTitle() {
      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    private fun initActionbar() {
        val imgContainer = LayoutInflater.from(context).inflate(R.layout.header_overlay, null)
        val img : ImageView = imgContainer.findViewById(R.id.img)
        img.setImageResource(R.drawable.parking_header)
        activity.setToCollapsingHeader(imgContainer)
        activity.expandCollapseAppbar(true)
    }

    private fun initBottomButton() {
        val bottomContainer = activity.findViewById<ViewGroup>(R.id.bottomContainer)
        bottomContainer.removeAllViews()
        LayoutInflater.from(activity).inflate(R.layout.floating_btn, bottomContainer)
        val fab : View = bottomContainer.findViewById(R.id.faBtn)
        fab.setOnClickListener {
            (activity as MainActivity).getRouter().navigateTo(Screens.NewTaskFragment()) }
    }

    private fun init() {
        showProgress()
        val manager = LinearLayoutManager(context)
        rv.layoutManager = manager
        homeMainAdapter = HomeMainAdapter(context)
        homeMainAdapter.setOnTaskClickListener(this)
        rv.adapter = homeMainAdapter
        presenter.attachView(this)
        presenter.getHomeData()

    }


    override fun onDestroyView() {
        super.onDestroyView()
        val bottomContainer = activity.findViewById<ViewGroup>(R.id.bottomContainer)
        bottomContainer.removeAllViews()
    }

    override fun onHomeDataLoaded(list: List<HomeWidgetTasks>) {
        hideProgress()
        homeMainAdapter.items = list
        rv.adapter?.notifyDataSetChanged()
    }

       // (activity as MainActivity).getRouter().navigateTo(Screens.NewTaskFragment(task.number))
       override fun onItemClick(type: String, task: TaskShort) {
           (activity as MainActivity).getRouter().navigateTo(Screens.TaskListFragment(type, task.owner))
       }
}
