package com.example.rootproject.ui.home


import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.mvp.implementation.BasePresenterImpl
import kotlinx.coroutines.*
import javax.inject.Inject


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class HomeRootPresenter @Inject constructor() : BasePresenterImpl<HomeRootEvents.View>(),
    HomeRootEvents.Presenter {

    override fun getHomeData() {
        DBgetTaskStatistics { getView()?.onHomeDataLoaded(it) }
    }
}