package com.example.rootproject.ui.main

import android.view.View
import com.example.rootproject.mvp.implementation.BasePresenterImpl
import com.example.rootproject.mvp.interfaces.BaseView
import javax.inject.Inject

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/19/19
*/
class MainPresenter @Inject constructor() : BasePresenterImpl<MainActivityEvents.View>(), MainActivityEvents.Presenter {


    override fun destroy() {
    }

    override fun viewIsReady(): Boolean {
        return false
    }


}