package com.example.rootproject.ui.home

import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

interface TaskListEvents {
    interface View : BaseView {
        fun onTasksLoaded(title : String,list: List<TaskInfoTable>)
    }

    interface Presenter: BasePresenter<View>  {
        fun getTaskList(type: String, ownerName : String)
    }
}