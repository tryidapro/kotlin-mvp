package com.example.rootproject.ui.base

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.rootproject.R
import com.example.rootproject.mvp.interfaces.BaseView
import com.google.android.material.appbar.AppBarLayout
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/11/19
*/

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector, BaseView {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }


    override fun supportFragmentInjector() = dispatchingAndroidInjector

    override fun showProgress() {
        var progress : View? = findViewById(R.id.progressGlobal)
        progress?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        var progress : View? = findViewById(R.id.progressGlobal)
        progress?.visibility = View.GONE
    }


    public fun setToCollapsingHeader(header: View) {
        val titleContainer : ViewGroup = findViewById(R.id.collapsingContainer) ?: return
        titleContainer.removeAllViews()
        titleContainer.addView(header)
    }

    public fun expandCollapseAppbar(expand: Boolean) {
        val appbar : AppBarLayout = findViewById(R.id.appbar)
        if (appbar != null) {
            appbar!!.setExpanded(expand, false)
        }
    }





}