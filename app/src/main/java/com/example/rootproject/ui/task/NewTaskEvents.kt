package com.example.rootproject.ui.home

import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

interface NewTaskEvents {

    interface View : BaseView {
        fun onTaskSaved()
        fun onTaskLoaded(type : String?, owner : String?, subj : String?, text: String?)

    }

    interface Presenter : BasePresenter<View> {
        fun saveTask(id : Int, type: String, owner: String, title : String, text : String)
        fun loadTask(number : Int)
        fun deleteTask(number : Int)
    }
}