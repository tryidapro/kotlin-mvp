package com.example.rootproject.ui.fragment1

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.rootproject.ui.main.MainActivity
import com.example.rootproject.R
import com.example.rootproject.cicerone.Screens


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/15/19
*/


class Fragment1 : Fragment() {


    private var color = R.color.primary_dark_material_dark


    companion object {
        private val FRAGMENT_NAME = "fragment_name"
        private val FRAGMENT_COLOR = "fragment_color"

        fun newInstance(name: String, number: Int): Fragment1 {
            val fragment = Fragment1()
            val arguments = Bundle()
            arguments.putString(FRAGMENT_NAME, name)
            arguments.putInt(FRAGMENT_COLOR, number)
            fragment.setArguments(arguments)

            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        color = arguments?.getInt(FRAGMENT_COLOR)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.home_root_layout, null)
        v.setBackgroundColor(ContextCompat.getColor(context as Context, color))

        return v
    }
}