package com.example.rootproject.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rootproject.R
import com.example.rootproject.adapters.HomeTabRecyclerAdapter
import com.example.rootproject.cicerone.Screens
import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.ui.base.BaseFragmentWithPresenter
import com.example.rootproject.ui.main.MainActivity
import kotlinx.android.synthetic.main.new_task_frg.*

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class NewTaskFragment : BaseFragmentWithPresenter<NewTaskEvents.View,NewTaskPresenter>(), NewTaskEvents.View {


    var v : View? = null
    var number : Int = 0


    companion object {
        fun newInstance(number : Int): NewTaskFragment {

            val fragment = NewTaskFragment()
            var bundle = Bundle()
            bundle.putInt("number", number)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        number = arguments!!.getInt("number",0)
     }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.new_task_frg, null)

        }
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        create_task.setOnClickListener { presenter.saveTask(number,type_spinner.selectedItem.toString(),
                                                    owner_spinner.selectedItem.toString(),
            subjTask.text.toString(),
                                                textTask.text.toString()) }
        if (number != 0) {
            showProgress()
            presenter.loadTask(number)
        }

        deleteButton.setOnClickListener { presenter.deleteTask(number) }
    }

    override fun onResume() {
        super.onResume()
        initActionbar()
    }



    private fun initActionbar() {
        activity.expandCollapseAppbar(false)
    }

     override fun showError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }



    override fun getTitle() {
    }

    override fun onTaskSaved() {
        hideProgress()
        (activity as MainActivity).getRouter().exit()
    }

    override fun onTaskLoaded(type : String?, owner : String?, subj : String?, text: String?){
        hideProgress()
        type_spinner.setSelection(resources.getStringArray(R.array.task_type).indexOf(type))
        owner_spinner.setSelection(resources.getStringArray(R.array.owner_type).indexOf(owner))
        owner_spinner.prompt = owner
        subjTask.setText(subj)
        textTask.setText(text)
    }
}