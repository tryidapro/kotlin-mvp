package com.example.rootproject.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.R
import com.example.rootproject.adapters.HomeMainAdapter
import com.example.rootproject.adapters.TasksListAdapter
import com.example.rootproject.cicerone.Screens
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.TaskModel.TaskShort
import com.example.rootproject.ui.base.BaseFragmentWithPresenter
import com.example.rootproject.ui.main.MainActivity
import kotlinx.android.synthetic.main.home_root_layout.*
import kotlinx.android.synthetic.main.task_list_frg.*

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class TaskListFragment : BaseFragmentWithPresenter<TaskListEvents.View,TaskListPresenter>(), TaskListEvents.View, TasksListAdapter.TabListEvents{

    var v : View? = null
    lateinit var taskListAdapter : TasksListAdapter

    var type : String = "null"
    var owner : String = "null"



    private var color = R.color.primary_dark_material_dark


    companion object {
        fun newInstance(type : String, owner : String): TaskListFragment {
            val fragment = TaskListFragment()
            var bundle = Bundle()
            bundle.putString("type", type)
            bundle.putString("owner", owner)
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        type = arguments!!.getString("type","null")
        owner = arguments!!.getString("owner","null")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.task_list_frg, null)
        }

        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onResume() {
        super.onResume()
        initActionbar()
        initBottomButton()
        showProgress()
        presenter.getTaskList(type, owner)
     }

    override fun onPause() {
        super.onPause()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun showError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }


     override fun getTitle() {
      //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun initActionbar() {
        val imgContainer = LayoutInflater.from(context).inflate(R.layout.header_overlay, null)
        val img : ImageView = imgContainer.findViewById(R.id.img)
        img.setImageResource(R.drawable.parking_header)
        activity.setToCollapsingHeader(imgContainer)
        activity.expandCollapseAppbar(true)
    }

    private fun initBottomButton() {
        val bottomContainer = activity.findViewById<ViewGroup>(R.id.bottomContainer)
        bottomContainer.removeAllViews()
        LayoutInflater.from(activity).inflate(R.layout.floating_btn, bottomContainer)
        val fab : View = bottomContainer.findViewById(R.id.faBtn)
        fab.setOnClickListener {
            (activity as MainActivity).getRouter().navigateTo(Screens.NewTaskFragment()) }
    }




    override fun onDestroyView() {
        super.onDestroyView()
        val bottomContainer = activity.findViewById<ViewGroup>(R.id.bottomContainer)
        bottomContainer.removeAllViews()
    }




    override fun onTasksLoaded(title: String, list: List<TaskInfoTable>) {
        hideProgress()
        taskListTitle.text = title
        val manager = LinearLayoutManager(context)
        taskListRv.layoutManager = manager
        taskListAdapter = TasksListAdapter()
        taskListAdapter.items = list
        taskListAdapter.setOnTaskClickListener(this)
        taskListRv.adapter = taskListAdapter
   }

    override fun onItemClick(task: TaskInfoTable) {
        (activity as MainActivity).getRouter().navigateTo(Screens.NewTaskFragment(task.taskId))
    }

    override fun loadMoreTasks() {
    }
}
