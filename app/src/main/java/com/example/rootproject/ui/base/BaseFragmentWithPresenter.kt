package com.example.rootproject.ui.base

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.rootproject.DB.CRUDInterface
import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.mvp.implementation.BasePresenterImpl
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView
import com.example.rootproject.ui.main.MainActivity
import com.example.rootproject.ui.main.MainActivityEvents
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/18/19
*/

abstract class BaseFragmentWithPresenter<V : BaseView, P : BasePresenter<V>> : BaseFragment() {

    lateinit var activity : BaseActivity
    @Inject
    lateinit var presenter: P
    @Inject
    lateinit var dbImpl : CRUDInterface

    @Inject
    lateinit var parentPresenter : MainActivityEvents.Presenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity = getActivity() as BaseActivity
        presenter.attachView(this as V)
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this as V)
    }

    override fun showProgress() {
        activity.showProgress()
    }

    override fun hideProgress() {
        activity.hideProgress()
    }

}