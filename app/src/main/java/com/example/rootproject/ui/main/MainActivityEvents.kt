package com.example.rootproject.ui.main

import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/25/19
*/

interface MainActivityEvents {
    interface View: BaseView {

    }

    interface Presenter : BasePresenter<View> {

    }
}