package com.example.rootproject.ui.main

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.widget.Toolbar
import com.example.rootproject.R
import com.example.rootproject.cicerone.Screens
import com.example.rootproject.ui.base.BaseActivityWithPresenter
import com.google.android.material.appbar.CollapsingToolbarLayout
import ru.terrakok.cicerone.*
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command


class MainActivity : BaseActivityWithPresenter<MainActivityEvents.View,MainPresenter>(), MainActivityEvents.View {


    lateinit var cicerone: Cicerone<Router>

    lateinit var toolbar : Toolbar

    private val navigator =
        object : SupportAppNavigator(
            this as androidx.fragment.app.FragmentActivity,
            R.id.content_frame
        ) {
            override fun applyCommands(commands: Array<out Command>?) {
                super.applyCommands(commands)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        cicerone = Cicerone.create()
        getRouter().navigateTo(Screens.HomeFragment())
        init()
    }

    fun getNavigatorHolder(): NavigatorHolder {
        return cicerone.navigatorHolder
    }

    fun getRouter(): Router {
        return cicerone.router
    }



    override fun onResume() {
        super.onResume()
        this.getNavigatorHolder().setNavigator(navigator)

    }

    override fun onPause() {
        super.onPause()
        this.getNavigatorHolder().removeNavigator()
    }



    override fun showError(error: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun init() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val collapsingToolbar : CollapsingToolbarLayout = findViewById(R.id.collapsing_toolbar)
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.collapsedtoolbar)
        collapsingToolbar.setExpandedTitleTextAppearance(R.style.expandedtoolbar)
        collapsingToolbar.setExpandedTitleMarginStart(resources.getDimensionPixelSize(R.dimen.toolbar_title_expanded_margin))
    }



}
