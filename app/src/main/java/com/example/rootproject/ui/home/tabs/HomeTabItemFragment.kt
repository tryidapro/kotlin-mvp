package com.example.rootproject.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.rootproject.R
import com.example.rootproject.adapters.HomeTabRecyclerAdapter
import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.ui.base.BaseFragmentWithPresenter

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class HomeTabItemFragment : BaseFragmentWithPresenter<HomeTabItemEvents.View,HomeTabItemPresenter>(), HomeTabItemEvents.View, HomeTabRecyclerAdapter.TabListEvents {



    lateinit var type : HomeTabList.TabsType

    var v : View? = null
    var recyclerView : RecyclerView? = null
    var adapter : HomeTabRecyclerAdapter? = null


    companion object {
        fun newInstance(type : HomeTabList.TabsType): HomeTabItemFragment {
            val args = Bundle()
            args.putString("type", type.name)
            val fragment = HomeTabItemFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fromArgs = arguments?.getString("type")!!
        type = HomeTabList.TabsType.valueOf(fromArgs)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (v == null) {
            v = inflater.inflate(R.layout.home_tab_frg, null)
            init()
        }
        return v
    }

    private fun init() {
        recyclerView = v?.findViewById(R.id.rv)
        val manager = LinearLayoutManager(context)
        recyclerView?.setLayoutManager(manager)
        adapter = HomeTabRecyclerAdapter()
        adapter?.setOnTaskClickListener(this)
        recyclerView?.setAdapter(adapter)
        presenter.attachView(this)
    }

    override fun onItemClick(task: Task) {
    }

    override fun showError(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun showProgress() {
    }

    override fun getTitle() {
    }

    override fun hideProgress() {
    }

    override fun loadMoreTasks() {

    }

    override fun onResume() {
        super.onResume()
        if (adapter != null) {
            adapter?.notifyDataSetChanged()
            if (adapter?.getItemCount() === 0) {
                presenter.loadMoreItems(type)
            }
        } else {
            presenter.loadMoreItems(type)
        }
    }

    override fun onTasksLoaded(items: List<Task>) {
        adapter?.addItems(items)
    }


    override fun setTasks(items: List<Task>) {
        adapter?.setItems(items)
    }
}