package com.example.rootproject.ui.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.rootproject.mvp.interfaces.BaseView
import dagger.android.support.AndroidSupportInjection

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/18/19
*/

abstract class BaseFragment : Fragment(), BaseView {
    abstract fun getTitle()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)
    }
}