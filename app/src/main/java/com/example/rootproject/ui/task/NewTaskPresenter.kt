package com.example.rootproject.ui.home

import androidx.core.util.Consumer
import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.mvp.implementation.BasePresenterImpl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import org.reactivestreams.Subscriber
import javax.inject.Inject
import kotlin.coroutines.suspendCoroutine


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class NewTaskPresenter @Inject constructor() : BasePresenterImpl<NewTaskEvents.View>(),
    NewTaskEvents.Presenter {

    override fun saveTask(id: Int, type: String, owner: String, title: String, text: String) {
        getView()?.showProgress()
        if (id != 0) {
            DBdeleteTask(id) { saveTask(0, type, owner, title, text) }
        }
        DBsaveTask(type, owner, title, text) {
            getView()?.onTaskSaved()
            getView()?.hideProgress()
        }
    }

    override fun loadTask(number: Int) {
        DBgetFullTask(number) {
            getView()?.onTaskLoaded(it.taskType, it.taskOwnerName, it.taskSubj, it.taskText)
        }
    }


    override fun deleteTask(number: Int) {
        DBdeleteTask(number) {
            getView()?.onTaskSaved()
        }
    }

}