package com.example.rootproject.ui.home

import com.example.rootproject.model.TaskModel.Task
import com.example.rootproject.model.home.HomeTabList
import com.example.rootproject.mvp.implementation.BasePresenterImpl
import javax.inject.Inject



/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class HomeTabItemPresenter @Inject constructor(): BasePresenterImpl<HomeTabItemEvents.View>(), HomeTabItemEvents.Presenter {

    override fun loadMoreItems(type: HomeTabList.TabsType) {
        val list = ArrayList<Task>()
        list.add(Task())
        list.add(Task())
        list.add(Task())
        getView()?.setTasks(list)
    }
}