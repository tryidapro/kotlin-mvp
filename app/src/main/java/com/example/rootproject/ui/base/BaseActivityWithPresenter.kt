package com.example.rootproject.ui.base

import android.os.Bundle
import android.view.View
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView
import javax.inject.Inject

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/18/19
*/

abstract class BaseActivityWithPresenter<V : BaseView,P : BasePresenter<V>> : BaseActivity(){

    @Inject
    lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this as V)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            presenter.destroy()
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this as V)
    }
}