package com.example.rootproject.ui.home


import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.mvp.implementation.BasePresenterImpl
import javax.inject.Inject


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

class TaskListPresenter @Inject constructor() : BasePresenterImpl<TaskListEvents.View>(),
    TaskListEvents.Presenter {


    override fun getTaskList(type: String, ownerName: String) {
        DBgetTaskList(type, ownerName, ::onTaskLoaded)
    }

    fun onTaskLoaded(type: String, ownerName: String, task: List<TaskInfoTable>) {
        getView()?.onTasksLoaded(ownerName + " : " + type, task)
    }
}