package com.example.rootproject.ui.home

import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/29/19
*/

interface HomeRootEvents {
    interface View : BaseView {
        fun onForecastLoaded(city : String)
        fun onHomeDataLoaded(list: List<HomeWidgetTasks>)
    }

    interface Presenter: BasePresenter<View>  {
        fun getHomeData()
    }
}