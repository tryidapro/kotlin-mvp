package com.example.rootproject.DB.room

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class TaskInfoTable (
    @PrimaryKey(autoGenerate = true) val taskId : Int,
    val taskType : String,
    val taskOwnerName : String?,
    val taskSubj :String?,
    val taskText : String?
)