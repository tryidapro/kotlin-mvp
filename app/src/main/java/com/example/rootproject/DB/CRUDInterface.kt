package com.example.rootproject.DB

import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import io.reactivex.Observable
import io.reactivex.Single

interface CRUDInterface {
    suspend  fun getFullTask(id : Int) : TaskInfoTable
    suspend fun saveTask(type :String, owner : String, title : String,text:String)
    suspend fun updateTask(task : TaskInfoTable)
    suspend fun getOwnerTasksCount(owner : String,type : String) : Int
    suspend fun changeTaskOwner(id : Int, newOwner : String)
    suspend fun changeTaskType(id : Int, newType : String)
    suspend fun getTaskStatistics() : List<HomeWidgetTasks>
    suspend fun getTaskList(type : String, owner : String) : List<TaskInfoTable>
    suspend fun deleteTask(number : Int)

}