package com.example.rootproject.DB.room

import androidx.room.*
import com.example.rootproject.DB.CRUDInterface
import io.reactivex.Observable
import retrofit2.http.DELETE

@Dao
interface TaskDAO {

    @Query("SELECT * FROM TaskInfoTable WHERE taskId = :id")
    fun getFullTask(id: Int): TaskInfoTable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveTask(task: TaskInfoTable)

    @Update
    fun updateTask(task: TaskInfoTable)

    @Query("SELECT COUNT(taskId) FROM TaskInfoTable WHERE taskOwnerName = :owner AND taskType = :type")
    fun getOwnerTasksCount(owner: String, type : String) :Int

    @Query("UPDATE TaskInfoTable SET taskOwnerName = :newOwner WHERE taskId = :id")
    fun changeTaskOwner(id: Int, newOwner: String)

    @Query("UPDATE TaskInfoTable SET taskType = :newType WHERE taskId = :id")
    fun changeTaskType(id: Int, newType: String)

    @Query( "SELECT * FROM TaskInfoTable WHERE taskOwnerName = :owner AND taskType = :type")
    fun getTaskList(type: String , owner : String) : List<TaskInfoTable>

    @Query("DELETE FROM TaskInfoTable WHERE taskId = :number")
    fun deleteTask(number : Int)
}