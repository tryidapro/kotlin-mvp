package com.example.rootproject.DB.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = arrayOf(TaskInfoTable::class), version = 1)
abstract class TaskDatabase : RoomDatabase() {



    abstract fun taskDAO() : TaskDAO

    companion object {

        /**
         * The only instance
         */
        private var sInstance: TaskDatabase? = null


        /**
         * Gets the singleton instance of SampleDatabase.
         *
         * @param context The context.
         * @return The singleton instance of SampleDatabase.
         */
        @Synchronized
        fun getInstance(context: Context): TaskDatabase {
            if (sInstance == null) {
                sInstance = Room
                    .databaseBuilder(context.applicationContext, TaskDatabase::class.java, "taskDB")
                    .build()
            }
            return sInstance!!
        }
    }

}