package com.example.rootproject.DB.room

import android.content.Context
import com.example.rootproject.DB.CRUDInterface
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.model.TaskModel.TaskShort
import kotlinx.coroutines.*


class RoomImpl : CRUDInterface {

    var db: TaskDatabase

    constructor(context: Context) {
        db = TaskDatabase.getInstance(context)
    }


    override suspend fun getFullTask(id: Int): TaskInfoTable {
        delay(1500)
        return db.taskDAO().getFullTask(id)
    }


    override suspend fun saveTask(type: String, owner: String, title: String, text: String) {
        val task = TaskInfoTable(0, type, owner, title, text)
        db.taskDAO().saveTask(task)
    }

    override suspend fun updateTask(task: TaskInfoTable) {
        db.taskDAO().updateTask(task)
    }

    override suspend fun getOwnerTasksCount(owner: String, type: String): Int {
        return db.taskDAO().getOwnerTasksCount(owner, type)

    }


    override suspend fun changeTaskOwner(id: Int, newOwner: String) {
        db.taskDAO().changeTaskOwner(id, newOwner)
    }

    override suspend fun changeTaskType(id: Int, newType: String) {
        db.taskDAO().changeTaskOwner(id, newType)
    }

    fun loadStats(): List<HomeWidgetTasks> {
        var list: MutableList<HomeWidgetTasks> = mutableListOf<HomeWidgetTasks>()

        for (type in arrayListOf("Backlog", "Week", "Day", "Check", "Done")) {
            var task = HomeWidgetTasks()
            task.title = type
            for (name in arrayListOf("Wife", "Husband", "All", "Grandma")) {
                var taskShort = TaskShort()
                taskShort.number = db.taskDAO().getOwnerTasksCount(name, type)
                taskShort.owner = name
                task.list.add(taskShort)
            }
            list.add(task)
        }
        return list
    }

    override suspend fun getTaskStatistics(): List<HomeWidgetTasks> {
        return loadStats()
    }


    override suspend fun getTaskList(type: String, owner: String): List<TaskInfoTable> {
        return db.taskDAO().getTaskList(type, owner)
    }

    override suspend fun deleteTask(number: Int) {
        delay(1500)
        db.taskDAO().deleteTask(number)
    }
}