package com.example.rootproject.model

import com.fasterxml.jackson.annotation.JsonProperty

class ServerResponse {

    @JsonProperty("list")
    var list: List<ListInfo> = ArrayList()

    @JsonProperty("cod")
    var cod: String = "null"

    @JsonProperty("message")
    var message: String = "null"

    @JsonProperty("city")
    var city: City = City()


}