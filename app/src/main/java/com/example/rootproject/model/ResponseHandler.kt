package com.example.rootproject.model

interface ResponseHandler {
    fun success(data : ServerResponse)
    fun error(info : String)
    fun businessError()

}