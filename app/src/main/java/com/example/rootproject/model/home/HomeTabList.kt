package com.example.rootproject.model.home

class HomeTabList {

    enum class TabsType {
        home, work, shop, other
    }
}