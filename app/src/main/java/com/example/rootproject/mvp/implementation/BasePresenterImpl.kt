package com.example.rootproject.mvp.implementation

import com.example.rootproject.DB.CRUDInterface
import com.example.rootproject.DB.room.TaskInfoTable
import com.example.rootproject.model.ResponseHandler
import com.example.rootproject.model.ServerResponse
import com.example.rootproject.model.TaskModel.HomeWidgetTasks
import com.example.rootproject.mvp.interfaces.BasePresenter
import com.example.rootproject.mvp.interfaces.BaseView
import com.example.rootproject.network.ApiInterface
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.observers.DisposableObserver
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.reactivestreams.Subscriber
import javax.inject.Inject


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/25/19
*/

abstract class BasePresenterImpl<V : BaseView> : BasePresenter<V> {

    @Inject
    lateinit var apiClient: ApiInterface

    @Inject
    lateinit var dbClient: CRUDInterface

    private var compositeDisposable: CompositeDisposable? = null
    private var v: V? = null

    @Inject
    lateinit var apiInterface: ApiInterface

    override fun attachView(view: V) {
        this.v = view
    }

    override fun detachView(view: V) {
        this.v = null
    }

    override fun getView(): V? {
        return v
    }

    override fun destroy() {
        v = null
        compositeDisposable?.let { t -> t.clear() }
    }

    override fun viewIsReady(): Boolean {
        return (v != null)
    }

    protected fun loadData(
        handler: ResponseHandler,
        observable: Observable<ServerResponse>,
        showProgress: Boolean,
        showError: Boolean
    ) {
        compositeDisposable?.let { t -> t.clear() }

        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        if (viewIsReady()) {
            if (showProgress) {
                getView()?.showProgress()
            }
        }
        compositeDisposable?.let { t ->
            t.add(observable.subscribeWith(object : DisposableObserver<ServerResponse>() {
                override fun onComplete() {
                    if (viewIsReady()) {
                        getView()?.hideProgress()
                    }
                }

                override fun onError(e: Throwable) {
                    if (viewIsReady()) {
                        if (showError)
                            getView()!!.showError("error")
                        getView()?.hideProgress()
                    }
                }

                override fun onNext(response: ServerResponse) {
                    if (viewIsReady()) {
                        if (!response.cod.equals("null")) { //TODO fix for more suitable check
                            handler.success(response)
                        } else {
                            handler.businessError()
                        }
                        getView()?.hideProgress()
                    }
                }
            }))
        }
    }

    fun DBgetFullTask(number: Int, f: (task: TaskInfoTable) -> Unit) {
        CoroutineScope(context = Dispatchers.Default).launch {
            val task = dbClient.getFullTask(number)
            withContext(context = Dispatchers.Main) {
                f(task)
            }
        }
    }

    fun DBsaveTask(type: String, owner: String, title: String, text: String, f: () -> Unit) {
        CoroutineScope(context = Dispatchers.Default).launch {
            dbClient.saveTask(type, owner, title, text)
            withContext(context = Dispatchers.Main) {
                f()
            }
        }
    }

    fun DBgetTaskList(
        type: String,
        ownerName: String,
        f: (t: String, ownerName: String, List<TaskInfoTable>) -> Unit
    ) {
        CoroutineScope(context = Dispatchers.Default).launch {
            val task = dbClient.getTaskList(type, ownerName)
            withContext(context = Dispatchers.Main) {
                f(type, ownerName, task)
            }
        }
    }

    fun DBupdateTask(task: TaskInfoTable, f: () -> Unit) {
        CoroutineScope(context = Dispatchers.Default).launch {
            dbClient.updateTask(task)
            withContext(context = Dispatchers.Main) {
                f
            }
        }
    }

    fun DBgetOwnerTasksCount(owner: String, type: String, f: (Int) -> Unit) {
        CoroutineScope(context = Dispatchers.Default).launch {
            val number = dbClient.getOwnerTasksCount(owner, type)
            withContext(context = Dispatchers.Main) {
                f(number)
            }
        }
    }

    fun DBchangeTaskOwner(id: Int, newOwner: String, f: () -> (Unit)) {
        CoroutineScope(context = Dispatchers.Default).launch {
            dbClient.changeTaskOwner(id, newOwner)
            withContext(context = Dispatchers.Main) {
                f()
            }
        }
    }

    fun DBchangeTaskType(id: Int, newType: String, f: () -> (Unit)) {
        CoroutineScope(context = Dispatchers.Default).launch {
            dbClient.changeTaskType(id, newType)
            withContext(context = Dispatchers.Main) {
                f()
            }
        }
    }

    fun DBgetTaskStatistics(f: (List<HomeWidgetTasks>) -> (Unit)) {
        CoroutineScope(context = Dispatchers.Default).launch {
            val list = dbClient.getTaskStatistics()
            withContext(context = Dispatchers.Main) {
                f(list)
            }
        }
    }

    fun DBgetTaskList(type: String, owner: String, f: (List<TaskInfoTable>) -> (Unit)) {
        CoroutineScope(context = Dispatchers.Default).launch {
            val list = dbClient.getTaskList(type, owner)
            withContext(context = Dispatchers.Main) {
                f(list)
            }
        }
    }

    fun DBdeleteTask(number: Int, f: () -> (Unit)) {
        CoroutineScope(context = Dispatchers.Default).launch {
            dbClient.deleteTask(number)
            withContext(context = Dispatchers.Main) {
                f()
            }
        }
    }


}