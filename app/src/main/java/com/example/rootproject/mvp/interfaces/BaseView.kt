package com.example.rootproject.mvp.interfaces


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/25/19
*/
interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun showError(error : String)
}