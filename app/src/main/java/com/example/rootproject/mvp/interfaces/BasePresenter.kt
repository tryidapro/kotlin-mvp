package com.example.rootproject.mvp.interfaces

import android.view.View

/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/25/19
*/
interface BasePresenter<V> {

    fun attachView(view: V)

    fun detachView(view: V)

    fun getView() : V?

    fun destroy()

    fun viewIsReady(): Boolean
}