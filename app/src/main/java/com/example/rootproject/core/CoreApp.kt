package com.example.rootproject.core

import android.app.Activity
import android.app.Application
import com.example.rootproject.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


class CoreApp:  Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>


    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .sampleApplicationBind(this)
            .context(this)
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    //https://github.com/serapbercin/dagger-architecture/blob/master/app/src/main/java/serapbercin/com/myapplication/di/

}