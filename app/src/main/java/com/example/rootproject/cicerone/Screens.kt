package com.example.rootproject.cicerone

import androidx.fragment.app.Fragment
import com.example.rootproject.ui.home.HomeRootFragment
import com.example.rootproject.ui.home.NewTaskFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen


/*
  Created by Andrey Pahomov,
   mail.for.pahomov@gmail.com on 4/15/19
*/
class Screens {

    class HomeFragment : SupportAppScreen(){
        override fun getFragment(): Fragment {
            return HomeRootFragment.newInstance()
        }
    }

    class NewTaskFragment() : SupportAppScreen(){
        var number : Int = 0
        constructor(number : Int): this(){
                this.number = number
        }
        override fun getFragment(): Fragment {
            return com.example.rootproject.ui.home.NewTaskFragment.newInstance(number)
        }
    }


    class SelectTaskSubject : SupportAppScreen(){
        override fun getFragment(): Fragment {
            return com.example.rootproject.ui.home.NewTaskFragment.newInstance(0)
        }
    }

    class TaskListFragment() : SupportAppScreen(){
        var type : String = "null"
        var owner : String = "null"
        constructor(type : String, owner : String): this(){
            this.type = type
            this.owner = owner
        }
        override fun getFragment(): Fragment {
            return com.example.rootproject.ui.home.TaskListFragment.newInstance(type, owner)
        }
    }


}