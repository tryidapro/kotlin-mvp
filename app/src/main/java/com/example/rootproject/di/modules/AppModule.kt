package com.example.rootproject.di.modules

import android.content.Context
import com.example.rootproject.DB.CRUDInterface
import com.example.rootproject.DB.room.RoomImpl
import com.example.rootproject.di.scopes.ActivityScope
import com.example.rootproject.network.ApiDataClient
import com.example.rootproject.network.ApiInterface
import com.example.rootproject.ui.main.MainActivity
import com.example.rootproject.ui.main.MainActivityEvents
import com.example.rootproject.ui.main.MainPresenter
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton


@Module
abstract class AppModule {

      @ActivityScope
      @ContributesAndroidInjector(modules = [MainActivityModule::class])
      abstract fun mainActivityInjector(): MainActivity



    @Module
    companion object {
        @Singleton
        @JvmStatic
        @Provides
        fun mainPresenter(): MainActivityEvents.Presenter = MainPresenter()

        @Singleton
        @JvmStatic
        @Provides
        fun apiInterface(dbImpl : CRUDInterface): ApiInterface = ApiDataClient(dbImpl)

        @Singleton
        @JvmStatic
        @Provides
        fun roomInterface(context: Context): CRUDInterface = RoomImpl(context)
    }

}


