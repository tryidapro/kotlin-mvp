package com.example.rootproject.di.component

import android.content.Context
import com.example.rootproject.core.CoreApp
import com.example.rootproject.di.modules.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,
 AppModule::class])
interface AppComponent {

 fun inject(coreApp: CoreApp)

 @Component.Builder
 interface Builder {

  fun build(): AppComponent
  @BindsInstance
  fun context(context: Context): Builder

  @BindsInstance
  fun sampleApplicationBind(coreApp: CoreApp): Builder



 }


}