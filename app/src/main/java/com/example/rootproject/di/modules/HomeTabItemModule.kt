package com.example.rootproject.di.modules

import com.example.rootproject.di.scopes.FragmentScope
import com.example.rootproject.ui.home.NewTaskFragment
import com.example.rootproject.ui.home.NewTaskEvents
import com.example.rootproject.ui.home.NewTaskPresenter
import dagger.Binds
import dagger.Module

@Module
abstract class HomeTabItemModule {
    @Binds
    @FragmentScope
    abstract fun presenter(presenter: NewTaskPresenter): NewTaskEvents.Presenter

    @Binds
    @FragmentScope
    abstract fun getFragment(fragment: NewTaskFragment): NewTaskEvents.View
}