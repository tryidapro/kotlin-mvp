package com.example.rootproject.di.modules

import com.example.rootproject.di.scopes.FragmentScope
import com.example.rootproject.ui.home.*
import dagger.Binds
import dagger.Module

@Module
abstract class HomeRootModule {
    @Binds
    @FragmentScope
    abstract fun presenter(presenter: HomeRootPresenter): HomeRootEvents.Presenter

    @Binds
    @FragmentScope
    abstract fun getFragment(fragment: HomeRootFragment): HomeRootEvents.View
}