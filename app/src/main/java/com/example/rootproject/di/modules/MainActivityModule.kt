package com.example.rootproject.di.modules

import com.example.rootproject.di.scopes.FragmentScope
import com.example.rootproject.ui.home.HomeRootFragment
import com.example.rootproject.ui.home.HomeTabItemFragment
import com.example.rootproject.ui.home.NewTaskFragment
import com.example.rootproject.ui.home.TaskListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {


    @FragmentScope
    @ContributesAndroidInjector(modules = [HomeRootModule::class])
    abstract fun homeRootInjector(): HomeRootFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [HomeTabItemModule::class])
    abstract fun homeTabItemInjector(): HomeTabItemFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [NewTaskModule::class])
    abstract fun newTaskInjector(): NewTaskFragment


    @FragmentScope
    @ContributesAndroidInjector(modules = [TasksListModule::class])
    abstract fun taskListInjector(): TaskListFragment

}